console.log("script is working");

// 1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

/* Всі об'єкти в JS мають приховану властивість [Prototype] - посилання на інші об'єкти.
 При створені нового об'єкту за допомогою наслідування використовується властивість [Prototype],
 яка робить посилання на той об'єкт в якого ми хочемо взяти потрібні властивості, тобто при наслідуванні
  створюються ланцюжки посилань. Якщо властивість [Prototype] не має значеня(посилання на інший об'єкт),
  а дорівнює null то цей об'єкт є фінальним в лацюжку посилань і немає прототипу з якого він наслідувався.
*/

//2) Для чого потрібно викликати super() у конструкторі класу-нащадка?
/*
Для того щоб викликати батьківський конструктор
*/ 



class Employee {
    constructor(name, age, salary){
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name() {
        return this._name
    }
    set name(value) {
        this._name = value
    }

    get age() {
        return this._age
    }
    set age(value) {
        this._age = value
    }

    get salary() {
        return this._salary
    }
    set salary(value) {
        this._salary = value
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }
    get salary() {
        return this._salary * 3
    }
}
const programmer1 = new Programmer("Arnold", 25, 3000, "eng")
const programmer2 = new Programmer("Jacki Chan", 25, 1000, "ua")
const programmer3 = new Programmer("Van Dam", 25, 2000, "pol")

console.log(programmer1.salary)
console.log(programmer2.salary)
console.log(programmer3.salary)